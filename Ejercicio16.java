import java.util.Scanner;

public class Ejercicio16 {

    public static void main(String[] args) {
 
        try (Scanner sc = new Scanner(System.in)) {
			int n;
			System.out.print("Introduce un numero: ");                                                         
			n = sc.nextInt();
			System.out.println("Tabla del " + n);
			for(int i = 1; i<=10; i++){
			     System.out.println(n + " * " + i + " = " + n*i);                                                     
			}
		}
    }
}