import java.util.Scanner;

public class Ejercicio3 {
	public static void main(String[] args) {
		try (Scanner reader = new Scanner (System.in)) {
			System.out.println("Ingrese el numero del mes numero: ");
			
			int numero = 0;
			numero = reader.nextInt();

			if (numero == 1 ) {
				System.out.println("Eligio enero, tiene 31");
			} else if (numero == 2) {
				System.out.println("Eligio febrero, tiene 28 días o 29 en año bisiesto");
			} else if (numero == 3) {
				System.out.println("Eligio marzo, tiene 31");
			} else if (numero == 4) {
				System.out.println("Eligio abril, tiene 3");
			} else if (numero == 5) {
				System.out.println("Eligio mayo, tiene 31");
			} else if (numero == 6) {
				System.out.println("Eligio junio, tiene 30");
			} else if (numero == 7) {
				System.out.println("Eligio julio, tiene 31");
			} else if (numero == 8) {
				System.out.println("Eligio agosto, tiene 31");
			} else if (numero == 9) {
				System.out.println("Eligio septiembre, tiene 30");
			} else if (numero == 10) {
				System.out.println("Eligio octubre, tiene 31");
			} else if (numero == 11) {
				System.out.println("Eligio noviembre, tiene 30");
			} else if (numero == 12) {
				System.out.println("Eligio diciembre, tiene 31");
			}	
		}
		}

		}