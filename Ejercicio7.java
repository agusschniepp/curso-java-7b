import java.util.Scanner;
public class Ejercicio7 {
public static void main(String[] args) {
    try (Scanner sc = new Scanner(System.in)) {
		int maximo = 3;
		int num;
		int mayor=0;
		int cont;
 
 
		for(cont = 0;cont < maximo;cont++ ){
		    System.out.println("Inserte un numero: ");
		    num = sc.nextInt();
		    if(num>mayor)
		    {
		        mayor=num;
		    }
		}
		System.out.println("El valor mayor es "+mayor);
	}
	}
}