import java.util.Scanner;

public class Ejercicio13 {
	 public static void main(String[] args){
		 try (Scanner reader = new Scanner(System.in)) {
			System.out.println("Ingrese el numero del mes numero:9");
			 
			  int mes = 0;
			  mes = reader.nextInt();  
			    String mesString;
			     
			    switch (mes) 
			    {
			        case 1:  mesString = "Eligio enero, tiene 31";
			                 break;
			        case 2:  mesString = "Eligio febrero, tiene 28 días o 29 en año bisiesto";
			                 break;
			        case 3:  mesString = "Eligio marzo, tiene 31";
			                 break;
			        case 4:  mesString = "Eligio abril, tiene 3";
			                 break;
			        case 5:  mesString = "Eligio mayo, tiene 31";
			                 break;
			        case 6:  mesString = "Eligio junio, tiene 30";
			                 break;
			        case 7:  mesString = "Eligio julio, tiene 31";
			                 break;
			        case 8:  mesString = "Eligio agosto, tiene 31";
			                 break;
			        case 9:  mesString = "Eligio septiembre, tiene 30";
			        		 break;
			        case 10: mesString = "Eligio octubre, tiene 31";
				 			 break;
			        case 11: mesString = "Eligio noviembre, tiene 30";
			        		 break;
			        default: mesString = "Eligio diciembre, tiene 31";
				 			 break;
			    }
			    System.out.println(mesString);
		}
	    }
	}